/* eslint-disable prettier/prettier */
export interface Custom {
    name: string;
    age: number;
    breed: string;
}


export interface Response {
    message: string,
    data: object,
}

export interface PostModel {
    id?: number;
    date: Date;
    title: string;
    body: string;
    category: string;
  }