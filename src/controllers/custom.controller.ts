/* eslint-disable prettier/prettier */
import {
  Controller,
  Get,
  Post,
  Patch,
  Delete,
  Body,
} from '@nestjs/common';
import { CreateCustomDto } from "../dtos/custom.dto"
import { CustomService } from '../services/custom.service';
import { Response } from "../interfaces/custom.interfaces";
@Controller('custom')
export class CustomController {
  constructor(private customService: CustomService) { }

  @Get('cats')
  findAll(): string {
    return 'This action returns all cats';
  }
  @Get()
  getCustomFunc(): Response {
    const data: CreateCustomDto = {
      name: "rozer",
      age: 26,
      breed: "human"
    };
    const res: Response = {
      message: 'This action adds a new cat',
      data
    }
    return res;
    // return this.customService.getCustomFunctions();
  }
  @Post()
  create(@Body() createCustomData: CreateCustomDto): Response {
    const res: Response = {
      message: 'This action adds a new cat',
      data: createCustomData,
    }
    this.customService.create(createCustomData)
    return res;
  }
  @Patch()
  update(): string {
    return 'This action adds a new cat';
  }
  @Delete()
  delete(): string {
    return 'This action adds a new cat';
  }
}
