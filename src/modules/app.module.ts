/* eslint-disable prettier/prettier */
import { Module, MiddlewareConsumer, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from '../controllers/app.controller';
import { AppService } from '../services/app.service';
import { CustomModule } from './custom.module';
import { LoggerMiddleware } from '../middlewares/loggger.middleware';

@Module({
  imports: [CustomModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule{
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('/');
    consumer.apply(LoggerMiddleware).forRoutes({path:"custom", method: RequestMethod.GET });
  }
}
