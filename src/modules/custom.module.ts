import { Module } from '@nestjs/common';
import { CustomController } from '../controllers/custom.controller';
import { CustomService } from '../services/custom.service';

@Module({
  imports: [],
  controllers: [CustomController],
  providers: [CustomService],
})
export class CustomModule {}
