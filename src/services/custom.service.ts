/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { Custom } from '../interfaces/custom.interfaces';

@Injectable()
export class CustomService {
    private readonly custom: Custom[] = [];
    getCustomFunctions(): string {
        return 'I am a custom modules';
    }

    create(cat: Custom) {
        this.custom.push(cat);
    }

    findAll(): Custom[] {
        return this.custom;
    }
}
