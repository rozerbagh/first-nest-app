import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app.module';
// import { CustomModule } from './modules/custom.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // const custom_app = await NestFactory.create(CustomModule);
  await app.listen(3004);
  // await custom_app.listen(3005);
}
bootstrap();
